# rn-utils

Utilities for React Native projects

## Installation

In package.json, this the following line
```json
"dependencies": {
    "rn-utils": "git+https://gitlab.com/karonatourn/rn-utils.git#v0.2.0"
},
```

## Document
Read [here](/docs/index.html)

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
