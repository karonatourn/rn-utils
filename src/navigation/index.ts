import React, { createRef } from 'react';
import {
  CommonActions,
  NavigationAction,
  NavigationContainerRef,
  NavigationState,
  PartialState,
  EventArg,
} from '@react-navigation/native';
import type { StackScreenProps } from '@react-navigation/stack';

export const navigationRef = createRef<NavigationContainerRef>();

type RootStackParamList = {
  AnyScreen: any;
};

type AnyScreenNavigationProp = StackScreenProps<
  RootStackParamList,
  'AnyScreen'
>;

export function addFocusListener(
  component: React.Component<AnyScreenNavigationProp>,
  listener: () => void
) {
  return component.props.navigation.addListener('focus', listener);
}

export function addBlurListener(
  component: React.Component<AnyScreenNavigationProp>,
  listener: () => void
) {
  return component.props.navigation.addListener('blur', listener);
}

export function addTransitionStartListener(
  component: React.Component<AnyScreenNavigationProp>,
  listener: (
    e: EventArg<
      'transitionStart',
      undefined,
      {
        closing: boolean;
      }
    >
  ) => void
) {
  return component.props.navigation.addListener('transitionStart', listener);
}

export function addTransitionEndListener(
  component: React.Component<AnyScreenNavigationProp>,
  listener: (
    e: EventArg<
      'transitionEnd',
      undefined,
      {
        closing: boolean;
      }
    >
  ) => void
) {
  return component.props.navigation.addListener('transitionEnd', listener);
}

export function isClosingTransition(
  event: EventArg<
    'transitionEnd',
    undefined,
    {
      closing: boolean;
    }
  >
) {
  return event.data.closing;
}

export function navigate(routeName: string, params: object) {
  navigationRef.current?.dispatch(
    CommonActions.navigate({
      name: routeName,
      params,
    })
  );
}

export function dispatchNavigationAction(
  action: NavigationAction | ((state: NavigationState) => NavigationAction)
) {
  navigationRef.current?.dispatch(action);
}

export function isActiveRoute(
  state: NavigationState | PartialState<NavigationState> | undefined,
  routeName: string
): boolean {
  if (
    !state ||
    state.index === undefined ||
    state.index === null ||
    state.routes === undefined ||
    state.routes === null ||
    state.routes.length === 0
  ) {
    return false;
  }

  if (state.routes[state.index]?.name === routeName) {
    return true;
  } else {
    return isActiveRoute(state.routes[state.index].state, routeName);
  }
}

export function setRouteParams(routeKey: string, params: object) {
  dispatchNavigationAction({
    ...CommonActions.setParams(params),
    source: routeKey,
  });
}
