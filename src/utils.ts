import { Dimensions, Platform, Linking, Share } from 'react-native';
import qs from 'qs';
import _ from 'lodash';
import type { LatLng, ImagePickerResponse } from './types';

/**
 * Check if remote debug is enabled
 */
export function isRemoteDebuggingEnabled() {
  const isEnabled = typeof atob !== 'undefined';
  return isEnabled;
}

export function getWindowDimensions() {
  return Dimensions.get('window');
}

export function getScreenDimensions() {
  return Dimensions.get('screen');
}

export function isIOS() {
  return Platform.OS === 'ios';
}

export function isAndroid() {
  return Platform.OS === 'android';
}

export function regexMatch(text: string, search: string) {
  const regex = new RegExp(
    '^[\\u0000-\\uFFFF\\s\\.\\t\\d\\w-]*' +
      search +
      '[\\u0000-\\uFFFF\\s\\.\\t\\d\\w-]*$',
    'gi'
  );
  return regex.test(text);
}

export function isFalseValue(value: any) {
  return (
    value === null ||
    value === undefined ||
    value === NaN ||
    value === false ||
    value === 0 ||
    value === '' ||
    value.length === 0
  );
}

export function isAnyFalseValue(...values: any[]) {
  return values.some((v) => isFalseValue(v));
}

export function isNullOrUndefined(value: any) {
  return value === null || value === undefined;
}

export function createHitSlop(offset: number) {
  return {
    left: offset,
    right: offset,
    top: offset,
    bottom: offset,
  };
}

/**
 * Open phone call with provided number
 *
 * @param {string} phoneNumber
 * @param {boolean} prompt
 */
export function callPhone(phoneNumber: string, prompt: boolean) {
  if (prompt && isIOS()) {
    return Linking.openURL(`telprompt:${phoneNumber}`);
  } else {
    return Linking.openURL(`tel:${phoneNumber}`);
  }
}

export function clamp01(value: number) {
  if (value > 1.0) {
    return 1.0;
  } else if (value < 0) {
    return 0;
  } else {
    return value;
  }
}

export function clamp(value: number, min: number, max: number) {
  if (value < min) {
    return min;
  } else if (value > max) {
    return max;
  } else {
    return value;
  }
}

/**
 * Convert data of image picker response to available 64 based data that can be sent
 * through http request
 * @param {object} imagePickerResponse Response data object from react native image picker
 */
export function get64BaseHttpImageData(
  imagePickerResponse: ImagePickerResponse
) {
  const data = imagePickerResponse.data ?? imagePickerResponse.base64;

  if (!data) {
    return null;
  }

  return `data:${imagePickerResponse.type};base64,${data}`;
}

export function share(dataUrl: string) {
  if (isAndroid()) {
    Share.share({
      message: dataUrl,
      title: 'Share',
    });
  } else {
    Share.share({
      url: dataUrl,
    });
  }
}

export function mailTo(email: string) {
  Linking.openURL('mailto:' + email);
}

export async function sendEmail(
  to: string,
  subject: string,
  body: string,
  options: {
    cc: string;
    bcc: string;
  }
) {
  const { cc, bcc } = options;

  let url = `mailto:${to}`;

  // Create email link query
  const query = qs.stringify({
    subject: subject,
    body: body,
    cc,
    bcc,
  });

  if (query.length) {
    url += `?${query}`;
  }

  // check if we can use this link
  const canOpen = await Linking.canOpenURL(url);

  if (!canOpen) {
    throw new Error('Provided URL can not be handled');
  }

  return Linking.openURL(url);
}

export function launchGoogleMapDirection({
  origin,
  destination,
}: {
  origin: LatLng;
  destination: LatLng;
}) {
  if (!destination) {
    return;
  }

  let url = 'https://www.google.com/maps/dir/?api=1';
  if (origin) {
    url = `${url}&origin=${origin.latitude},${origin.longitude}`;
  }

  if (destination) {
    url = `${url}&destination=${destination.latitude},${destination.longitude}`;
  }

  Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        return false;
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => console.log('An error occurred', err));
}

export function launchGoogleTurnByTurnNavigation({
  latitude,
  longitude,
}: LatLng) {
  // const url = `google.navigation:q=${latitude},${longitude}`;
  // const url = `http://maps.apple.com/?ll=${latitude},${longitude}`;
  // const url = `maps:0,0?q=Custom Label@${latitude},${longitude}`;
  const scheme = Platform.select({
    android: 'google.navigation:q=',
    ios: 'maps://app?daddr=',
  });

  const url = `${scheme}${latitude},${longitude}`;

  Linking.canOpenURL(url)
    .then((supported) => {
      if (!supported) {
        console.log("Can't handle url: " + url);
        return false;
      } else {
        return Linking.openURL(url);
      }
    })
    .catch((err) => console.log('An error occurred', err));
}

export function isIphoneX() {
  const { width, height } = getWindowDimensions();
  return isIOS() && (height === 812 || width === 812);
}

export function isImagePath(path: string) {
  return path.search(/.jpg|png|jpeg|bmp|tiff|JPG|PNG|JPEG|BMP|TIFF$/) >= 0;
}

function log(
  logFunc: (message: string, ...optionalParams: any[]) => void,
  message: string,
  ...optionalParams: any[]
) {
  if (!isRemoteDebuggingEnabled() || typeof logFunc !== 'function') {
    return;
  }
  logFunc(message, ...optionalParams);
}

export function logInfo(message: string, ...optionalParams: any[]) {
  log(console.log, message, ...optionalParams);
}

export function logWarn(message: string, ...optionalParams: any[]) {
  log(console.warn, message, ...optionalParams);
}

export function logError(message: string, ...optionalParams: any[]) {
  log(console.error, message, ...optionalParams);
}

/**
 * Check if the string value contains only numeric digits
 * @param {string} value
 */
export function isNumeric(value: string) {
  const reg = /[^0-9]/gi;
  return !reg.test(value);
}

export function formatNumberWithCommas(num: string) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function removeNonDigitCharacters(text: string) {
  return text.replace(/[^0-9\.,]/g, '').replace(',', '.');
}

export function parseOnlyNumericString(value: string) {
  return value.replace(/\D/, '');
}

/**
 * Api
 * -------------------------------------------------------------------------
 */

/**
 * Convert query paramters to string
 * Ex: { id: 1, usrname: "john" } => 'id=1&username=john'
 * @param {object} params { key: value, ... }
 */
export function convertQueryParamtersToString(params: {
  [key: string]: number | string;
}) {
  const ret = [];
  if (params) {
    for (let d in params) {
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(params[d]));
    }
  }
  return ret.join('&');
}

/**
 * Called to get value index of probabilities
 * @param {array} probs Array of number
 */
export function getProbability(probs = []) {
  let total = 0;

  for (let p of probs) {
    total += p;
  }

  let randomPoint = Math.random() * total;
  let i = 0;

  for (i = 0; i < probs.length; i++) {
    if (randomPoint < probs[i]) {
      break;
    } else {
      randomPoint -= probs[i];
    }
  }

  return i;
}

/**
 * Path
 * -------------------------------------------------------------------------
 */
export function getFileExtension(filename: string) {
  return filename.split('.').pop();
}

export function getFileNameWithoutExtension(filename: string) {
  const st = filename.split('/');
  return st[st.length - 1].split('.').shift();
}

export function getFileName(filePath: string) {
  const st = filePath.split('/');
  return st[st.length - 1];
}

/**
 * Use to get YouTube video id from its URL
 * @param youtubeUrl YouTube URL
 * @returns YouTube video id
 */
export function getYouTubeVideoId(youtubeUrl: string) {
  const regExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
  const match = youtubeUrl.match(regExp);
  const videoId = match && match[1].length === 11 ? match[1] : false;
  return videoId;
}

export function getYouTubeUrlById(youtubeId: string) {
  return `https://youtu.be/${youtubeId}`;
}

export function isYouTubeLink(url: string) {
  return new RegExp('youtube', 'gi').test(url);
}

/**
 * Android
 * -------------------------------------------------------------------------
 */
export function getAndroidBottomNavigationBarHeight() {
  return getScreenDimensions().height - getWindowDimensions().height;
}

/**
 * iOS Settings
 * References:
 * https://stackoverflow.com/questions/39081688/open-settings-app-from-another-app-in-ios-react-native
 * https://stackoverflow.com/questions/8246070/ios-launching-settings-restrictions-url-scheme
 * -------------------------------------------------------------------------
 */
function openiOSAppSettings() {
  return Linking.openURL('app-settings:');
}

function openiOSSettings(type: string, path?: string) {
  let url = `App-Prefs:root=${type}`;
  if (!isFalseValue(path)) {
    url = `${url}&path=${path}`;
  }
  return Linking.openURL(url);
}

const iOSSettingType = {
  General: 'General',
  WiFi: 'WIFI',
  About: 'About',
  AirplaneMode: 'AIRPLANE_MODE',
  Accessibility: 'ACCESSIBILITY',
};

export const iOSSettings = {
  openAppSettings: openiOSAppSettings,
  openGeneralSettings: () => openiOSSettings(iOSSettingType.General),
  openAbout: () =>
    openiOSSettings(iOSSettingType.General, iOSSettingType.About),
  openWiFiSettings: () => openiOSSettings(iOSSettingType.WiFi),
  openAirplaneMode: () => openiOSSettings(iOSSettingType.AirplaneMode),
  openAccessibility: () =>
    openiOSSettings(iOSSettingType.General, iOSSettingType.Accessibility),
};

export const capitalize = (str = '') => _.startCase(_.toLower(str));
