import { useEffect } from 'react';
import usePrevious from './usePrevious';
import type { CallbackFunction } from './types';

export default function useDidUpdate(value: any, callback: CallbackFunction) {
  const oldValue = usePrevious(value);

  useEffect(() => {
    if (oldValue !== undefined && oldValue !== value) {
      callback && callback(value);
    }
  }, [value, oldValue, callback]);
}
