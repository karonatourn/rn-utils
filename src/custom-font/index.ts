import { StyleProp, StyleSheet, TextStyle } from 'react-native';
import type { CustomFont } from './types';

var _customFonts: CustomFont = null;
var _cachedFontFamily: {
  [key: string]: string;
} = {};

function setCustomFonts(customFonts: CustomFont) {
  _customFonts = customFonts;
}

function getFontFamily(
  baseFontName: string | undefined | null,
  fontWeight: string | number | undefined | null,
  fontStyle: string | undefined | null
) {
  if (!_customFonts || !baseFontName) {
    return null;
  }

  if (!fontWeight) {
    fontWeight = 'normal';
  }

  if (!fontStyle) {
    fontStyle = 'normal';
  }

  const cacheKey = `${baseFontName}${fontWeight}${fontStyle}`;

  let fontFamily = _cachedFontFamily[cacheKey];

  if (fontFamily) {
    return fontFamily;
  }

  const font = _customFonts[baseFontName];

  if (!font) {
    return null;
  }

  let weight = null;
  fontFamily = baseFontName;

  if (fontWeight && font.weight) {
    weight = font.weight[fontWeight];

    if (weight) {
      fontFamily = `${fontFamily}-${weight}`;
    }
  }

  if (fontStyle && font.style) {
    const style = font.style[fontStyle];

    if (style && fontStyle !== 'normal') {
      fontFamily = weight ? `${fontFamily}${style}` : `${fontFamily}-${style}`;
    }
  }

  _cachedFontFamily[cacheKey] = fontFamily;

  return fontFamily;
}

function getCustomTextStyle(
  referenceTextStyle: StyleProp<TextStyle>
): StyleProp<TextStyle> {
  if (!referenceTextStyle) {
    return referenceTextStyle;
  }

  const { fontFamily, fontStyle, fontWeight, ...rest } = StyleSheet.flatten(
    referenceTextStyle
  );

  const customFontFamily = getFontFamily(fontFamily, fontWeight, fontStyle);

  if (!customFontFamily) {
    if (fontFamily) {
      return referenceTextStyle;
    } else {
      return { fontStyle, fontWeight, ...rest };
    }
  }

  return {
    ...rest,
    fontFamily: customFontFamily,
  };
}

export { setCustomFonts, getCustomTextStyle };
