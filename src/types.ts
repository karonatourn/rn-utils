import type { ImagePickerResponse as BaseImagePickerResponse } from 'react-native-image-picker';

export type LatLng = {
  latitude: number;
  longitude: number;
};

export interface ImagePickerResponse extends BaseImagePickerResponse {
  data?: string;
}
